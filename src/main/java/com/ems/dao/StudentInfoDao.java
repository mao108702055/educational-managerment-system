package com.ems.dao;

import com.ems.entity.StudentInfo;
import org.apache.ibatis.annotations.Select;

import java.util.Map;

public interface StudentInfoDao {

    @Select("select b.id from edusystem.t_user a left join edusystem.t_student b on a.no = b.u_no where b.id = #{id}")
    Map findStudentById(int id);
    /**
     * 通过编号和姓名展示对应的学生信息
     * @return
     */
    @Select("select s.id, s.name sname, g.name cname, s.sex, s.birthday, s.email, s.phone, s.qq, s.schoolname, s.education from edusystem.t_student s left join edusystem.t_grade g on s.u_no = g.u_no where s.id = #{id}")
    StudentInfo findStudentByIdAndName(int id);
}

