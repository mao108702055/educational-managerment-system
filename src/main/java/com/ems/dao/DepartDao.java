package com.ems.dao;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface DepartDao {
    /**
     * 分组查询 每个部门人数
     * @return
     */
    @Select("select count(1) num, b.name from edusystem.t_staff a left join edusystem.t_depart b on a.d_id = b.id group by b.name")
    List<Map> getCountByDepart();


}
