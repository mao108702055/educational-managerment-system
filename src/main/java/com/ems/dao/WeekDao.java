package com.ems.dao;

import com.ems.entity.Week;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface WeekDao {
    /**
     * 展示所有
     * @return
     */
    @Select("select * from edusystem.t_week")
    List<Week> findAllWeek();

    /**
     * 添加信息
     * @return
     */
    @Insert(value = "insert into edusystem.t_week (title, content) values (#{title}, #{content})")
    int addWeek(@Param("title") String title, @Param("content") String content);

    /**
     * 根据id修改信息
     * @param week
     * @return
     */
    @Update("update edusystem.t_week set title = #{title}, content = #{content}, status = #{status}, createTime = #{createTime}, u_no = #{u_no} where id = #{id}")
    int updateWeek(Week week);

    /**
     * 根据id查找信息
     * @param id
     * @return
     */
    @Select(value = "select * from edusystem.t_week where id = #{id}")
    Week findWeekById(int id);
    /**
     * 根据id删除信息
     * @param id
     * @return
     */

    @Delete("delete from edusystem.t_week where id = #{id}")
    int deleteWeekById(int id);
}
