package com.ems.dao;

import com.ems.entity.Skill;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SkillDao {
    /**
     * 添加信息
     * @param content
     * @return
     */
    @Insert(value = "insert into edusystem.t_skill(content) values (#{content})")
    int addSkill(@Param("content") String content);

    /**
     * 展示所有
     * @return
     */
    @Select("select * from edusystem.t_skill")
    List<Skill> findAllSkill();
}
