package com.ems.dao;

import com.ems.entity.Vip;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface VipDao {
    /**
     * 添加信息
     * @param content
     * @return
     */
    @Insert(value = "insert into edusystem.t_vip(content) values (#{content})")
    int addVip(@Param("content") String content);

    /**
     * 展示所有
     * @return
     */
    @Select("select * from edusystem.t_vip")
    List<Vip> findVip();
}
