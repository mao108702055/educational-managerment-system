package com.ems.dao;

import com.ems.entity.Student;
import org.apache.ibatis.annotations.Insert;

public interface StudentDao {
    @Insert("insert into edusystem.t_student (u_no, name, sex, birthday, cardno, schoolname, education, phone, email, qq, gno, flag, entrytime) VALUES (#{u_no}, #{name}, #{sex}, #{birthday}, #{cardno}, #{schoolname}, #{education}, #{phone}, #{email}, #{qq}, #{gno}, #{flag}, #{entrytime})")
    int addStudent(Student student);
}
