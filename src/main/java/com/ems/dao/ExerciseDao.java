package com.ems.dao;

import com.ems.entity.Exercise;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface ExerciseDao {

    /**
     * 添加习题
     * @param exercise
     * @return
     */
    @Insert("insert into edusystem.t_exercises (category, equestion, eanswer, flag) values (#{category}, #{equestion}, #{eanswer}, #{flag})")
    int addExercises(Exercise exercise);

    /**
     * 查询所有习题
     * @return
     */
    @Select("select * from edusystem.t_exercises")
    List<Exercise> getAllExercises();

    /**
     * 删除所选id习题
     * @param id
     * @return
     */
    @Delete("delete from edusystem.t_exercises where id = #{id}")
    int deleteExerciseById(int id);

}
