package com.ems.dao;

import com.ems.entity.Question;

import java.util.List;


public interface ResourceDao {

    //添加学习笔记
    int addNote(Question question);

    //添加代码
    int addCode(Question question);

    //增加资源
    int addResource(Question question);

    //查询所有资源
    List<Question> findAllResource();

    //删除资源
    int deleteResource(int id);

    //更改资源
    int updateResource(Question question);

}
