package com.ems.dao;

import com.ems.entity.Anonymity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AnonymityDao {
    /**
     * 添加投诉内容
     * @param content
     * @return
     */
    @Insert(value = "insert into edusystem.t_anonymity(content) values (#{content})")
    int addAnonymity(@Param("content") String content);

    /**
     * 展示所有信息
     * @return
     */
    @Select("select * from edusystem.t_anonymity")
    List<Anonymity> findAllAnonymity();

}
