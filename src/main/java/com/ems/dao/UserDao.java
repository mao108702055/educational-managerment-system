package com.ems.dao;

import com.ems.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserDao {

    /**
     * 根据账号和密码查询学生
     * @param no
     * @param password
     * @return
     */
    // 去数据库查询用户名和密码
    @Select("select * from edusystem.t_user where no = #{no} and password = #{password}")
    List<User> findUserByNOAndPassword(@Param("no") String no,@Param("password") String password);



}
