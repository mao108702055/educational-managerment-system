package com.ems.dao;

import com.ems.entity.Grade;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface GradeDao {


    @Select("select * from edusystem.t_grade g left join edusystem.t_course c on c.id=g.u_no")
    List<Grade> getAllGrade();

    @Select("select name from edusystem.t_grade")
    List<String> getAllGradeName();

    @Delete("delete from edusystem.t_grade where id = #{id}")
    int deleteGradeById(int id);

    @Update("update edusystem.t_grade set name = #{name}, createtime = #{createtime}, week = #{week}, location = #{location} where id = #{id}")
    int updateGrade(Grade course);

    @Insert("insert into edusystem.t_grade (u_no, name, createtime, week, location) VALUES (#{u_no}, #{name}, #{createtime}, #{week}, #{location})")
    int insertGrade(Grade grade);




}
