package com.ems.dao;

import com.ems.entity.Attendance;
import com.ems.entity.AttendanceInfo;
import com.ems.entity.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;


public interface AttendanceDao {

    /**
     * 申请请假
     * @param attendance
     * @return
     */
    @Insert("insert into edusystem.t_askforleave (uid, startTime, endTime, count, status, reason) VALUES (#{uid}, #{startTime}, #{endTime}, #{count}, #{status}, #{reason})")
    int addAttendance(Attendance attendance);

    /**
     * 如果role = 1 员工 查询该用户id对应得姓名
     * @param id
     * @return
     */
    @Select("select a.no,b.name from edusystem.t_user a left join edusystem.t_staff b on a.no = b.u_no where a.id = #{id}")
    Map findStaffById(int id);

    /**
     * 如果role =2 学生 查询该用户id对应得姓名
     * @param id
     * @return
     */
    @Select("select a.no,b.name from edusystem.t_user a left join edusystem.t_student b on a.no = b.u_no where a.id = #{id};")
    Map findStudentById(int id);

    @Select("select a.id, startTime, endTime, count, status, reason, b.name from edusystem.t_askforleave a left join edusystem.t_student b on a.uid = b.id ")
    List<AttendanceInfo> findAllAskForLeave();

    @Delete("delete from edusystem.t_askforleave where id = #{id}")
    int deleteAskForLeaveById(int id);

}
