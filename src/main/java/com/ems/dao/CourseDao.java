package com.ems.dao;

import com.ems.entity.Course;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface CourseDao {
    @Insert("insert into edusystem.t_course (name, createtime, week, type) VALUES (#{name}, #{createtime}, #{week}, #{type})")
    int addCourse(Course course);

    @Select("select * from edusystem.t_course")
    List<Course> getAllCourse();

    @Delete("delete from edusystem.t_course where id = #{id}")
    int deleteCourseById(int id);

    @Update("update edusystem.t_course set name = #{name}, createtime = #{createtime}, week = #{week}, type = #{type}, flag = #{flag} where id = #{id}")
    int updateCourse(Course course);

}
