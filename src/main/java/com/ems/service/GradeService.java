package com.ems.service;

import com.ems.entity.Grade;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GradeService {


    /**
     * 展示班级列表
     * @return
     */
    List<Grade> getAllGrade();

    /**
     * 更新班级信息
     * @param grade
     * @return
     */
    int updateGrade(Grade grade);

    /**
     * 删除班级
     * @param id
     * @return
     */
    int deleteGradeById(int id);

    /**
     * 获取所有班级的名字
     * @return
     */
    List<String> getAllGradeName();

    /**
     * 增加班级
     * @param grade
     * @return
     */
    int insertGrade(Grade grade);
}
