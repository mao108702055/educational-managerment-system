package com.ems.service;

import com.ems.entity.GroupDepartData;
import org.springframework.stereotype.Service;

@Service
public interface DepartService {
    /**
     * 获取 部门分组人数
     * @return
     */
    GroupDepartData getCountByDepart();
}
