package com.ems.service;

import com.ems.entity.Student;
import com.ems.entity.StudentInfo;

public interface StudentInfoService {

    /**
     * 查询学生id
     * @param id
     * @return
     */
    int findStudentById(int id);
    /**
     * 根据编号和姓名查找学生信息
     * @return
     */
    StudentInfo findStudentByIdAndName(int sid);
}
