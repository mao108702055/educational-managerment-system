package com.ems.service;

import com.ems.entity.Skill;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SkillService {
    /**
     * 添加
     * @param content
     * @return
     */
    int addSkill(@Param("content") String content);

    /**
     * 展示
     * @return
     */
    List<Skill> findAllSkill();
}
