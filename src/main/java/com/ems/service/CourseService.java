package com.ems.service;

import com.ems.entity.Course;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CourseService {
    /**
     * 添加课程
     * @param course
     * @return
     */
    int addCourse(Course course);

    /**
     * 展示课程列表
     * @return
     */
    List<Course> getAllCourse();

    /**
     * 根据id删除课程
     * @param id
     * @return
     */
    boolean deleteCourseById(int id);

    /**
     * 对课程进行编辑
     * @param course
     * @return
     */
    int updateCourse(Course course);
}
