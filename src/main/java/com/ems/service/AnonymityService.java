package com.ems.service;

import com.ems.entity.Anonymity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AnonymityService {
    /**
     * 添加信息
     * @param content
     * @return
     */
    int addAnonymity(@Param("content") String content);

    /**
     * 展示
     * @return
     */
    List<Anonymity> findAllAnonymity();
}
