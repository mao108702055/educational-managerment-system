package com.ems.service;

import com.ems.entity.Attendance;
import com.ems.entity.AttendanceInfo;
import com.ems.entity.User;

import java.util.List;

public interface AttendanceService {
    /**
     * 添加请假
     * @param attendance
     * @return
     */
    int addLeave(Attendance attendance);


    /**
     * 查询员工姓名
     * @param user
     * @return
     */
    String findNameByNo(User user);

    /**
     * 查看出勤列表
     * @return
     */
    List<AttendanceInfo> getAttendanceList();

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteAskForLeave(int id);
}
