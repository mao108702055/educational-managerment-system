package com.ems.service;

import com.ems.entity.Vip;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VipService {
    /**
     * 添加
     * @param content
     * @return
     */
    int addVip(@Param("content") String content);

    /**
     * 展示所有
     * @return
     */
    List<Vip> findVip();
}
