package com.ems.service.Impl;

import com.ems.dao.SkillDao;
import com.ems.entity.Skill;
import com.ems.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SkillServiceImpl implements SkillService {
    @Autowired
    private SkillDao skillDao;
    @Override
    public int addSkill(String content) {
        return skillDao.addSkill(content);
    }

    @Override
    public List<Skill> findAllSkill() {
        return skillDao.findAllSkill();
    }
}
