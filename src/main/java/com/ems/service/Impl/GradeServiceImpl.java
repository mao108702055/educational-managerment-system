package com.ems.service.Impl;

import com.ems.dao.GradeDao;
import com.ems.entity.Grade;
import com.ems.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

@Service
public class GradeServiceImpl implements GradeService {

    @Autowired
    private GradeDao gradeDao;


    @Override
    public List<Grade> getAllGrade() {
        return gradeDao.getAllGrade();
    }

    @Override
    public int updateGrade(Grade grade) {
        return gradeDao.updateGrade(grade);
    }

    @Override
    public int deleteGradeById(int id) {
        return gradeDao.deleteGradeById(id);
    }

    @Override
    public List<String> getAllGradeName() {
        return gradeDao.getAllGradeName();
    }

    @Override
    public int insertGrade(Grade grade) {
        return gradeDao.insertGrade(grade);
    }


}
