package com.ems.service.Impl;

import com.ems.dao.ExerciseDao;
import com.ems.entity.Exercise;
import com.ems.service.ExerciseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExerciseServiceImpl implements ExerciseService {
    @Autowired
    private ExerciseDao exerciseDao;

    /**
     * 添加习题
     * @param exercise 练习对象
     * @return
     */
    @Override
    public int addExercises(Exercise exercise) {
        return exerciseDao.addExercises(exercise);
    }

    /**
     * 查询所有习题
     * @return
     */
    @Override
    public List<Exercise> getAllExercises() {
        return exerciseDao.getAllExercises();
    }

    /**
     * 删除所选id习题
     * @param id 习题id
     * @return 更新行数
     */
    @Override
    public int deleteExerciseById(int id) {
        return exerciseDao.deleteExerciseById(id);
    }


}
