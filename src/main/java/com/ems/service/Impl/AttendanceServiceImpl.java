package com.ems.service.Impl;

import com.ems.dao.AttendanceDao;
import com.ems.entity.Attendance;
import com.ems.entity.AttendanceInfo;
import com.ems.entity.Student;
import com.ems.entity.User;
import com.ems.service.AttendanceService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class AttendanceServiceImpl implements AttendanceService {

    @Autowired
    private AttendanceDao attendanceDao;


    @Override
    public int addLeave(Attendance attendance) {
        return attendanceDao.addAttendance(attendance);
    }

    @Override
    public String findNameByNo(User user) {
        String name = null;
        if (user.getRole() == 1) {
            Map staff = attendanceDao.findStaffById(user.getId());
            name = (String) staff.get("name");

        }
        else if(user.getRole() == 2){
            Map student = attendanceDao.findStudentById(user.getId());
            name = (String) student.get("name");
        }
        return name;
    }

    @Override
    public List<AttendanceInfo> getAttendanceList() {

        return attendanceDao.findAllAskForLeave();
    }

    @Override
    public int deleteAskForLeave(int id) {
        return attendanceDao.deleteAskForLeaveById(id);
    }


}
