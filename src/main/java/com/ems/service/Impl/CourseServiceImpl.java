package com.ems.service.Impl;

import com.ems.dao.CourseDao;
import com.ems.entity.Course;
import com.ems.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl  implements CourseService {
    @Autowired
    private CourseDao courseDao;

    @Override
    public int addCourse(Course course) {
        return courseDao.addCourse(course);
    }

    @Override
    public List<Course> getAllCourse() {
        return courseDao.getAllCourse();
    }

    @Override
    public boolean deleteCourseById(int id) {
        int num = courseDao.deleteCourseById(id);
        if (num > 0) {
            return true;
        }
        return false;
    }

    @Override
    public int updateCourse(Course course) {
        return courseDao.updateCourse(course);
    }
}
