package com.ems.service.Impl;

import com.ems.dao.WeekDao;
import com.ems.entity.Week;
import com.ems.service.WeekService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeekServiceImpl implements WeekService {
    @Autowired
    private WeekDao weekDao;
    @Override
    public List<Week> findAllWeek() {
        return weekDao.findAllWeek();
    }

    @Override
    public int addWeek(String title, String content) {
        return weekDao.addWeek(title, content);
    }

    @Override
    public Week findWeekById(int id) {
        return weekDao.findWeekById(id);
    }

    @Override
    public int updateWeek(Week week) {
        return weekDao.updateWeek(week);
    }

    @Override
    public boolean deleteWeekById(int id) {
        int i = weekDao.deleteWeekById(id);
        if (i > 0) {
            return true;
        }
        return false;
    }
}
