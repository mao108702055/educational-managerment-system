package com.ems.service.Impl;

import com.ems.dao.VipDao;
import com.ems.entity.Vip;
import com.ems.service.VipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VipServiceImpl implements VipService {
    @Autowired
    private VipDao vipDao;
    @Override
    public int addVip(String content) {
        return vipDao.addVip(content);
    }

    @Override
    public List<Vip> findVip() {
        return vipDao.findVip();
    }
}
