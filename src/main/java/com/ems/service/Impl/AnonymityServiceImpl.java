package com.ems.service.Impl;

import com.ems.dao.AnonymityDao;
import com.ems.entity.Anonymity;
import com.ems.service.AnonymityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnonymityServiceImpl implements AnonymityService {
    @Autowired
    private AnonymityDao anonymityDao;
    @Override
    public int addAnonymity(String content) {
        return anonymityDao.addAnonymity(content);
    }

    @Override
    public List<Anonymity> findAllAnonymity() {
        return anonymityDao.findAllAnonymity();
    }
}
