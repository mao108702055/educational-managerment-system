package com.ems.service.Impl;

import com.ems.dao.DepartDao;
import com.ems.entity.GroupDepartData;
import com.ems.service.DepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class DepartServiceImpl implements DepartService {

    @Autowired
    private DepartDao departDao;

    /**
     * 获取部门分组人数
     * @return
     */
    @Override
    public GroupDepartData getCountByDepart() {
        List<Map> mapList = departDao.getCountByDepart();

        List<String> departList = new ArrayList<>();
        List<Integer> departNumList = new ArrayList<>();

        for (Map map : mapList) {
            departList.add((String) map.get("name"));
            Long num = (Long) map.get("num");
            departNumList.add(Math.toIntExact(num));
        }

        GroupDepartData groupDepartData = new GroupDepartData();
        // 部门名称列表
        groupDepartData.setDepartList(departList);
        // 对应部门人数列表
        groupDepartData.setDepartNumList(departNumList);

        return groupDepartData;
    }
}
