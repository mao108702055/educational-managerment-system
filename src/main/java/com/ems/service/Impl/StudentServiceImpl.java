package com.ems.service.Impl;

import com.ems.dao.StudentDao;
import com.ems.entity.Student;
import com.ems.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDao studentDao;

    @Override
    public int addStudent(Student student) {

        return studentDao.addStudent(student);
    }
}
