package com.ems.service;

import com.ems.entity.Exercise;

import java.util.List;

public interface ExerciseService {
    /**
     * 添加习题
     * @param exercise 练习对象
     * @return
     */
    int addExercises(Exercise exercise);

    /**
     * 查询习题
     * @return
     */
    List<Exercise> getAllExercises();

    /**
     * 删除所选id的习题
     * @param id
     * @return
     */
    int deleteExerciseById(int id);
}
