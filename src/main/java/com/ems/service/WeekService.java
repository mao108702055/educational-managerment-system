package com.ems.service;

import com.ems.entity.Week;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WeekService {
    /**
     * 展示所有
     * @return
     */
    List<Week> findAllWeek();

    /**
     * 添加信息

     * @return
     */
    int addWeek(@Param("title") String title, @Param("content") String content);

    /**
     * 根据id查找信息
     * @param id
     * @return
     */
    Week findWeekById(int id);
    /**
     * 根据id修改信息
     * @param week
     * @return
     */
    int updateWeek(Week week);

    /**
     * 根据id删除信息
     * @param id
     * @return
     */
    boolean deleteWeekById(int id);
}
