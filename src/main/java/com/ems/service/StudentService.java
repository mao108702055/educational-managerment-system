package com.ems.service;

import com.ems.entity.Student;
import org.springframework.stereotype.Service;


public interface StudentService {
    int addStudent(Student student);
}
