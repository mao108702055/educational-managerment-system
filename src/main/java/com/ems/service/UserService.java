package com.ems.service;


import com.ems.entity.User;
import java.util.List;

public interface UserService {
    /**
     * 根据账号和密码查询学生
     * @param no
     * @param password
     * @return
     */
    List<User> findUserByNoAndPassword(String no, String password);

}
