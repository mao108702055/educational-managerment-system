package com.ems.service;

import com.ems.entity.Question;

import java.util.List;


public interface ResourceService {

    //添加学习笔记
    boolean addNote(Question question);

    //添加代码
    boolean addCode(Question question);

    //增加资源
    boolean addResource(Question question);

    //查询所有资源
    List<Question> findAllResource();

    //删除资源
    boolean deleteResource(int id);

    //更改资源
    boolean updateResource(Question question);

}
