package com.ems.service.Impl;

import com.ems.dao.ResourceDao;
import com.ems.entity.Question;
import com.ems.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ResourceServiceImpl implements ResourceService {
    @Autowired
    private ResourceDao resourceDao;

    @Override  //添加笔记
    public boolean addNote(Question question) {
        int num = resourceDao.addNote(question);
        if (num>0){
            return true;
        }
        return false;
    }

    @Override   //添加代码
    public boolean addCode(Question question) {
        int num = resourceDao.addCode(question);
        if (num > 0) {
            return true;
        }
        return false;

    }

    @Override  //添加资源
    public boolean addResource(Question question) {
        int num = resourceDao.addResource(question);
        if (num > 0) {
            return true;
        }
        return false;
    }

    @Override  //查找所有资源
    public List<Question> findAllResource() {
        return resourceDao.findAllResource();
    }

    @Override  //删除资源
    public boolean deleteResource(int id) {
        int num = resourceDao.deleteResource(id);
        if (num > 0) {
            return true;
        }
        return false;
    }

    @Override  //修改资源
    public boolean updateResource(Question question) {
        int num = resourceDao.updateResource(question);
        if (num > 0) {
            return true;
        }
        return false;
    }
}
