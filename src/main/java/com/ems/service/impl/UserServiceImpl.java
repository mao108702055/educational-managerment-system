package com.ems.service.Impl;

import com.ems.dao.UserDao;
import com.ems.entity.User;
import com.ems.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;


    /**
     * 查询用户通过用户名和密码
     * @param no 用户名
     * @param password 密码
     * @return
     */
    @Override
    public List<User> findUserByNoAndPassword(String no, String password) {
        return userDao.findUserByNOAndPassword(no,password);
    }


}
