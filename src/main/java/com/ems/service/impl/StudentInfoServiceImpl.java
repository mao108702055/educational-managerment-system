package com.ems.service.Impl;

import com.ems.dao.StudentInfoDao;
import com.ems.entity.Student;
import com.ems.entity.StudentInfo;
import com.ems.service.StudentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class StudentInfoServiceImpl implements StudentInfoService {
    @Autowired
    private StudentInfoDao studentInfoDao;

    @Override
    public int findStudentById(int id) {
        Map map = studentInfoDao.findStudentById(id);

        int sid  = (int) map.get("id");
        return sid;
    }

    @Override
    public StudentInfo findStudentByIdAndName(int sid) {
        return studentInfoDao.findStudentByIdAndName(sid);
    }
}
