package com.ems.entity;

import java.io.Serializable;


/**
 * 相应前端数据的的结构 --转换为json
 * @param <T>
 */
public class ResponseData<T>  implements Serializable {

//"code": 0,  0 代表成功   1代表失败不渲染表格
//        "msg": "",  状态
//        "count": 1000,  查询结果总行数
//        "data":      数据集合

    private int code;

    private String msg;

    // 总行数
    private int count;


    private   T data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", count=" + count +
                ", data=" + data +
                '}';
    }
}
