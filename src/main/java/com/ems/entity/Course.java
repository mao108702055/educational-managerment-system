package com.ems.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class Course implements Serializable {

    private int id;

    private String name;

    // 将接收的json  数据转化为 date   不能接口 key value 字符串转化为date
    // 将Java对象 date 转化为 字符串  yyyy-MM-dd HH:mm:ss
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")// 接收key value 字符串 转化为 Date
    private Date createtime;

    private int week;

    private int type;

    private int flag;

    public Course() {
    }

    public Course(int id, String name, Date createtime, int week, int type, int flag) {
        this.id = id;
        this.name = name;
        this.createtime = createtime;
        this.week = week;
        this.type = type;
        this.flag = flag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", createtime=" + createtime +
                ", week=" + week +
                ", type=" + type +
                ", flag=" + flag +
                '}';
    }

}
