package com.ems.entity;

import java.io.Serializable;

public class User implements Serializable {


    private int id;

    /**
     * 账号
     */
    private String no;

    /**
     * 密码
     */
    private String password;


    /**
     * 角色 老师1 学生2
     */
    private  int role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", no='" + no + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}
