package com.ems.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


public class Grade implements Serializable {
    private int id;
    private String u_no;
    private String name;

    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")// 接收key value 字符串 转化为 Date
    private Date createtime;

    private int week;
    private String location;

    public Grade() {
    }

    public Grade(int id, String u_no, String name, Date createtime, int week, String location) {
        this.id = id;
        this.u_no = u_no;
        this.name = name;
        this.createtime = createtime;
        this.week = week;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getU_no() {
        return u_no;
    }

    public void setU_no(String u_no) {
        this.u_no = u_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "id=" + id +
                ", u_no='" + u_no + '\'' +
                ", name='" + name + '\'' +
                ", createtime=" + createtime +
                ", week=" + week +
                ", location='" + location + '\'' +
                '}';
    }

}
