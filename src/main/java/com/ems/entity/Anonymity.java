package com.ems.entity;

import java.io.Serializable;

public class Anonymity implements Serializable {
    private int id;
    private String content;
    private String u_no;

    public Anonymity() {
    }

    public Anonymity(int id, String content, String u_no) {
        this.id = id;
        this.content = content;
        this.u_no = u_no;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getU_no() {
        return u_no;
    }

    public void setU_no(String u_no) {
        this.u_no = u_no;
    }

    @Override
    public String toString() {
        return "Anonymity{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", u_no='" + u_no + '\'' +
                '}';
    }
}
