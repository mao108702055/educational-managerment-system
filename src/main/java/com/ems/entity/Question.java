package com.ems.entity;


public class Question {

    private int id;
    private String uNo;//用户
    private String question;//问题
    private String answer;//答案
    private String qtype;
    private int star;//星星
    private String flag;//资源或笔记或代码

    public Question() {
    }

    public Question(int id, String uNo, String question, String answer, String qtype, int star, String flag) {
        this.id = id;
        this.uNo = uNo;
        this.question = question;
        this.answer = answer;
        this.qtype = qtype;
        this.star = star;
        this.flag = flag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getuNo() {
        return uNo;
    }

    public void setuNo(String uNo) {
        this.uNo = uNo;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQtype() {
        return qtype;
    }

    public void setQtype(String qtype) {
        this.qtype = qtype;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", uNo='" + uNo + '\'' +
                ", question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", qtype='" + qtype + '\'' +
                ", star=" + star +
                ", flag='" + flag + '\'' +
                '}';
    }
}
