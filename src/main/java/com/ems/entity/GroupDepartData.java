package com.ems.entity;

import java.io.Serializable;
import java.util.List;

public class GroupDepartData implements Serializable {
    // 部门名称列表
    private List<String> departList;

    // 对应部门人数
    private List<Integer> departNumList;

    public List<String> getDepartList() {
        return departList;
    }

    public void setDepartList(List<String> departList) {
        this.departList = departList;
    }

    public List<Integer> getDepartNumList() {
        return departNumList;
    }

    public void setDepartNumList(List<Integer> departNumList) {
        this.departNumList = departNumList;
    }

    @Override
    public String toString() {
        return "GroupDepartData{" +
                "departList=" + departList +
                ", departNumList=" + departNumList +
                '}';
    }
}
