package com.ems.entity;

import java.io.Serializable;

public class Exercise implements Serializable {
    private int id;
    private String category;
    private String equestion;
    private String eanswer;
    private String flag;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEquestion() {
        return equestion;
    }

    public void setEquestion(String equestion) {
        this.equestion = equestion;
    }

    public String getEanswer() {
        return eanswer;
    }

    public void setEanswer(String eanswer) {
        this.eanswer = eanswer;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Exercise{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", equestion='" + equestion + '\'' +
                ", eanswer='" + eanswer + '\'' +
                ", flag='" + flag + '\'' +
                '}';
    }
}
