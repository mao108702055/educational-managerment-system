package com.ems.controller;

import com.ems.entity.Exercise;
import com.ems.entity.ResponseData;
import com.ems.service.ExerciseService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ExerciseController {
    @Autowired
    private ExerciseService exerciseService;

    @RequestMapping("/exer/add.do")
    public ResponseData addExercises(Exercise exercise) {

        exerciseService.addExercises(exercise);
        ResponseData responseData = new ResponseData();
        responseData.setCode(0);
        responseData.setMsg("success");

        return responseData;
    }
    @RequestMapping("/exer/select.do")
    public ResponseData getAllExercises(int page,int limit) {
        PageHelper.startPage(page,limit);
        List<Exercise> exerciseList = exerciseService.getAllExercises();
        PageInfo<Exercise> pageInfo = new PageInfo<>(exerciseList);
        int count = (int) pageInfo.getTotal();

        ResponseData responseData = new ResponseData();
        responseData.setCode(0);
        responseData.setMsg("success");
        responseData.setCount(count);
        responseData.setData(exerciseList);
        return responseData;
    }

    @RequestMapping("/exer/delete.do")
    public ResponseData deleteExerciseById(int id) {

        exerciseService.deleteExerciseById(id);
        ResponseData responseData = new ResponseData();
        responseData.setCode(0);
        responseData.setMsg("success");

        return responseData;
    }


    /**
     * 饼状图展示
     * @return
     */
    @RequestMapping("/findexercise.do")
    public ResponseData findExercises() {
        ResponseData responseData = new ResponseData();

        List<Exercise> exercisesList = exerciseService.getAllExercises();

        ArrayList<String> count1 = new ArrayList<>();
        ArrayList<String> count2 = new ArrayList<>();

        for (int i = 0; i < exercisesList.size(); i++) {
            count1.add(exercisesList.get(i).getCategory());
            count2.add(exercisesList.get(i).getEquestion());
        }

        // 去除重复的习题种类
        for (int i = 0; i < count1.size() - 1; i++) {
            for (int j = count1.size() - 1; j > i; j--) {
                if (count1.get(j).equals(count1.get(i))) {
                    count1.remove(j);
                }
            }
        }

        // 习题种类数量
        responseData.setCode(count1.size());

        // 习题问题和答案数量
        responseData.setCount(count2.size());

        return responseData;
    }

}
