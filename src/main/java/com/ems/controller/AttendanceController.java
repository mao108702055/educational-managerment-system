package com.ems.controller;

import com.ems.entity.Attendance;
import com.ems.entity.AttendanceInfo;
import com.ems.entity.ResponseData;
import com.ems.entity.User;
import com.ems.service.AttendanceService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class AttendanceController {

    @Autowired
    private AttendanceService attendanceService;

    @RequestMapping("/AskForLeave/insertLeave.do")
    public ResponseData insertLeave(Attendance attendance, HttpServletRequest req) {

        attendanceService.addLeave(attendance);
        ResponseData responseData = new ResponseData();
        responseData.setCode(0);
        responseData.setMsg("success");
        return responseData;
    }

    @RequestMapping("/OaNew/findByNoStu.do")
    public ResponseData findByNoStu(HttpServletRequest req) {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        String name = attendanceService.findNameByNo(user);


        ResponseData responseData = new ResponseData();

        responseData.setCode(0);
        responseData.setMsg("success");
        responseData.setData(name);
        return responseData;
    }

    @RequestMapping("/askLeaveList.do")
    public ResponseData findAllAttendance(int page, int limit) {
        PageHelper.startPage(page,limit);
        List<AttendanceInfo> attendanceList = attendanceService.getAttendanceList();
        PageInfo<AttendanceInfo> pageInfo = new PageInfo<>(attendanceList);
        int count = (int) pageInfo.getTotal();
        ResponseData responseData = new ResponseData();

        responseData.setCode(0);
        responseData.setData(attendanceList);
        responseData.setMsg("success");
        responseData.setCount(count);
        return responseData;
    }

    @RequestMapping("/deleteAskForLeave.do")
    public ResponseData deleteAskForLeave(int id) {
        attendanceService.deleteAskForLeave(id);
        ResponseData responseData = new ResponseData();

        responseData.setCode(0);
        responseData.setMsg("成功");
        return responseData;
    }
}
