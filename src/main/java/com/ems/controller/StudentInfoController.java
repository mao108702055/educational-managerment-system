package com.ems.controller;

import com.ems.entity.ResponseData;
import com.ems.entity.Student;
import com.ems.entity.StudentInfo;
import com.ems.entity.User;
import com.ems.service.StudentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class StudentInfoController {
    @Autowired
    private StudentInfoService studentInfoService;
    @RequestMapping("/user/queryStudent.do")
    public ResponseData findStudentByIdAndName(HttpServletRequest req){

        ResponseData responseData = new ResponseData();
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        int id= studentInfoService.findStudentById(user.getId());
        StudentInfo studentInfo = studentInfoService.findStudentByIdAndName(id);
        responseData.setData(studentInfo);
        responseData.setCode(0);
        responseData.setMsg("success");

        return responseData;
    }
}
