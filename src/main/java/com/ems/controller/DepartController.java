package com.ems.controller;

import com.ems.entity.GroupDepartData;
import com.ems.entity.ResponseData;
import com.ems.service.DepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepartController {

    @Autowired
    private DepartService departService;

    @RequestMapping("/groupByDept.do")
    public ResponseData getCountByDepart() {
        ResponseData responseData = new ResponseData();

        GroupDepartData groupDepartData = departService.getCountByDepart();
        responseData.setCode(0);
        responseData.setMsg("success");
        responseData.setData(groupDepartData);
        return responseData;
    }
}
