package com.ems.controller;

import com.ems.entity.ResponseData;
import com.ems.entity.User;
import com.ems.entity.Week;
import com.ems.service.WeekService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class WeekController {
    @Autowired
    private WeekService weekService;
    @RequestMapping("/getUser.do")
    public ResponseData getUserNo(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        ResponseData responseData = new ResponseData();
        responseData.setCode(0);
        responseData.setData(user.getNo());
        return responseData;
    }

    @RequestMapping("/add.do")
    public ResponseData addWeek(String title, String content) {

        int num = weekService.addWeek(title,content);
        ResponseData responseData = new ResponseData();
        if (num > 0) {
            responseData.setMsg("success");
            responseData.setCode(0);
        }else {
            responseData.setMsg("fail");
            responseData.setCode(1);
        }
        return responseData;
    }

    @RequestMapping("/query.do")
    public ResponseData<List<Week>> getAllWeek(HttpServletRequest request) {

        ResponseData<List<Week>> responseData = new ResponseData<>();
        List<Week> weekList = weekService.findAllWeek();
        responseData.setCode(0);
        responseData.setMsg("success");
        responseData.setData(weekList);
        return responseData;
    }
    @RequestMapping("/deleteById.do")
    public ResponseData deleteWeekById(int id) {
        boolean b = weekService.deleteWeekById(id);
        ResponseData responseData = new ResponseData();
        if (b) {
            responseData.setMsg("success");
            responseData.setCode(0);
        }else {
            responseData.setMsg("fail");
            responseData.setCode(1);
        }
        return responseData;
    }
    @RequestMapping("/update.do")
    public ResponseData updateWeek(Week week) {
        int num = weekService.updateWeek(week);
        ResponseData responseData = new ResponseData();
        if (num > 0) {
            responseData.setMsg("success");
            responseData.setCode(0);
        }else {
            responseData.setMsg("fail");
            responseData.setCode(1);
        }
        return responseData;
    }
    @RequestMapping("/queryWeekById.do")
    public ResponseData findWeekById(int id){
        Week weekById = weekService.findWeekById(id);
        ResponseData responseData = new ResponseData();
        responseData.setCode(0);
        responseData.setMsg("success");
        responseData.setData(weekById);
        return responseData;
    }

}
