package com.ems.controller;

import com.ems.entity.Grade;
import com.ems.entity.ResponseData;
import com.ems.service.GradeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 *
 */

@RestController
public class GradeController {

    @Autowired
    private GradeService gradeService;

    @RequestMapping("grade/selectGradeAll.do")
    public ResponseData<List<Grade>> getAllGrade(int page, int limit) {

        // 开启分页
        PageHelper.startPage(page, limit);

        // 按照分页查询
        List<Grade> gradeList = gradeService.getAllGrade();

        PageInfo<Grade> gradePageInfo = new PageInfo<>(gradeList);

        // 获取数据表中所有数据行数
        int count = (int) gradePageInfo.getTotal();

        ResponseData<List<Grade>> responseData = new ResponseData<>();

        responseData.setCode(0);
        responseData.setMsg("success");
        responseData.setCount(count);
        responseData.setData(gradeList);

        return responseData;
    }

    @RequestMapping("grade/deleteGradeById.do")
    public ResponseData deleteGradeById(int id) {

        int num = gradeService.deleteGradeById(id);

        ResponseData responseData = new ResponseData();

        if (num > 0) {
            responseData.setMsg("success");
            responseData.setCode(0);// 0代表成功
        } else {
            responseData.setMsg("fail");
            responseData.setCode(1);// 1 代表失败
        }
        return responseData;
    }

    //修改班级
    @RequestMapping("grade/updateGrade.do")
    public ResponseData updateGrade(Grade grade) {
        int num = gradeService.updateGrade(grade);

        ResponseData responseData = new ResponseData();

        if (num > 0) {
            responseData.setMsg("success");
            responseData.setCode(0);// 0代表成功
        } else {
            responseData.setMsg("fail");
            responseData.setCode(1);// 1 代表失败
        }
        return responseData;
    }

    //添加新班
    @RequestMapping("grade/insertGrade.do")
    public ResponseData insertGrade(Grade grade) {
        int num = gradeService.insertGrade(grade);

        ResponseData responseData = new ResponseData();

        if (num > 0) {
            responseData.setMsg("添加成功");
            responseData.setCode(0);// 0代表成功
        } else {
            responseData.setMsg("fail");
            responseData.setCode(1);// 1 代表失败
        }
        return responseData;
    }

}
