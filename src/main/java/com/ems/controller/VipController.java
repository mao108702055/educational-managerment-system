package com.ems.controller;

import com.ems.entity.ResponseData;
import com.ems.entity.User;
import com.ems.entity.Vip;
import com.ems.service.VipService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class VipController {
    @Autowired
    private VipService vipService;
    @RequestMapping("/http://localhost:8080/user/query.do")
    public ResponseData getUserNo(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        ResponseData responseData = new ResponseData();
        responseData.setCode(0);
        responseData.setData(user.getNo());
        return responseData;
    }
    @RequestMapping("/addVip.do")
    public ResponseData addVip(String content) {
        int i = vipService.addVip(content);
        ResponseData responseData = new ResponseData();
        if (i > 0) {
            responseData.setMsg("success");
            responseData.setCode(0);
        }else {
            responseData.setMsg("fail");
            responseData.setCode(1);
        }
        return responseData;
    }

    @RequestMapping("/vip/queryVip.do")
    public ResponseData<Vip> getAllVip(int page, int limit) {
        PageHelper.startPage(page,limit);

        List<Vip> vipList = vipService.findVip();

        PageInfo<Vip> pageInfo = new PageInfo<>(vipList);

        int count = (int) pageInfo.getTotal();
        ResponseData responseData = new ResponseData();

        responseData.setCode(0);
        responseData.setMsg("success");
        responseData.setCount(count);
        responseData.setData(vipList);

        return responseData;
    }
}
