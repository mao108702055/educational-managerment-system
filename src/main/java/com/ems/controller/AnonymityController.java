package com.ems.controller;

import com.ems.entity.Anonymity;
import com.ems.entity.ResponseData;
import com.ems.service.AnonymityService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AnonymityController {
    @Autowired
    private AnonymityService anonymityService;

    @RequestMapping("/anonymity/add.do")
    public ResponseData addAnonymity(String content) {
       // User user = (User) request.getSession().getAttribute("user");
        int i = anonymityService.addAnonymity(content);
        ResponseData responseData = new ResponseData();
        if (i > 0) {
            responseData.setMsg("success");
            responseData.setCode(0);
        }else {
            responseData.setMsg("fail");
            responseData.setCode(1);
        }
        return responseData;
    }
    @RequestMapping("/anonymity/queryAnonymity.do")
    public ResponseData<Anonymity> findAllAnonymity(int page, int limit) {
        PageHelper.startPage(page,limit);
        List<Anonymity> anonymityList = anonymityService.findAllAnonymity();
        PageInfo<Anonymity> pageInfo = new PageInfo<>(anonymityList);
        int count = (int) pageInfo.getTotal();

        ResponseData responseData = new ResponseData();
        responseData.setCode(0);
        responseData.setMsg("success");
        responseData.setCount(count);
        responseData.setData(anonymityList);
        return responseData;
    }
}
