package com.ems.controller;

import com.ems.entity.ResponseData;
import com.ems.entity.Student;
import com.ems.service.GradeService;
import com.ems.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {
    @Autowired
    private StudentService studentService;

    @Autowired
    private GradeService gradeService;

    @RequestMapping("/student/insertStudent.do")
    public ResponseData addStudent(Student student) {


        ResponseData responseData = new ResponseData();

        int num = studentService.addStudent(student);

        if (num > 0) {
            responseData.setMsg("success");
            responseData.setCode(0);// 0代表成功
        } else {
            responseData.setMsg("fail");
            responseData.setCode(1);// 1 代表失败
        }
        return responseData;
    }

    @RequestMapping("student/selectGrade.do")
    public ResponseData<List<String>> selectGrade() {

        List<String> gradeList = gradeService.getAllGradeName();

        ResponseData<List<String>> responseData = new ResponseData<>();

        responseData.setCode(0);
        responseData.setMsg("success");
        responseData.setData(gradeList);

        return responseData;
    }
}
