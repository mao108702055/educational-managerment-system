package com.ems.controller;

import com.ems.entity.Question;
import com.ems.entity.ResponseData;
import com.ems.entity.User;
import com.ems.service.ResourceService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class ResourceController {
    @Autowired
    private ResourceService resourceService;
    /**
     * 添加笔记
     * @param question
     * @param request
     * @return
     */
    @RequestMapping("/OaNew/addNote.do")//recourse.html
    public ResponseData addNote(Question question, HttpServletRequest request) {

        User user = (User) request.getSession().getAttribute("user");
        String no = user.getNo();
        question.setuNo(no);
        question.setQtype("笔记");
        question.setFlag("笔记");

        ResponseData responseData = new ResponseData();
        boolean result = resourceService.addNote(question);
        if (result) {
            responseData.setCode(1);
            responseData.setMsg("添加学习笔记成功");
        } else {
            responseData.setCode(0);
            responseData.setMsg("添加学习笔记失败");
        }
        return responseData;
    }



    /**
     * 添加代码
     * @param question
     * @param request
     * @return
     */
    @RequestMapping("/OaNew/addCode.do")
    public ResponseData addCode(Question question, HttpServletRequest request) {
        //获取用户
        User user = (User) request.getSession().getAttribute("user");
        String no = user.getNo();
        question.setuNo(no);
        question.setQtype("代码");
        question.setFlag("代码");

        ResponseData responseData = new ResponseData();
        boolean result = resourceService.addCode(question);
        if (result) {
            responseData.setCode(1);
            responseData.setMsg("添加代码成功");
        } else {
            responseData.setCode(0);
            responseData.setMsg("添加代码失败");
        }
        return responseData;


    }
    /**
     * 添加资源
     * @param question
     * @param request
     * @return
     */
    @RequestMapping("/OaNew/savequestion.do")
    public ResponseData addResource(Question question, HttpServletRequest request) {
        //获取用户
        User user = (User) request.getSession().getAttribute("user");
        String no = user.getNo();
        question.setuNo(no);
        question.setFlag("资源");

        ResponseData responseData = new ResponseData();
        boolean result = resourceService.addResource(question);
        if (result) {
            responseData.setCode(1);
            responseData.setMsg("添加资源成功");
        } else {
            responseData.setCode(0);
            responseData.setMsg("添加资源失败");
        }
        return responseData;
    }


    /**
     * 分页查询资源
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/OaNew/queryquestionbyno.do")
    public ResponseData<List<Question>> getAllResourceByPage(int page, int limit) {
        PageHelper.startPage(page, limit);// 开启分页

        List<Question> questionList = resourceService.findAllResource(); // 分页查询
        PageInfo<Question> questionPageInfo = new PageInfo<>(questionList);

        int count = (int) questionPageInfo.getTotal();   // 总页数

        ResponseData<List<Question>> responseData = new ResponseData<>();

        responseData.setCode(0);
        responseData.setMsg("success");
        responseData.setData(questionList);
        responseData.setCount(count);

        return responseData;
    }

    /**
     * 删除资源
     * @param id
     * @return
     */
    @RequestMapping("/OaNew/questiondelete.do")
    public ResponseData deleteResource(int id) {

        ResponseData responseData = new ResponseData();
        boolean result = resourceService.deleteResource(id);
        if (result) {
            responseData.setCode(1);
            responseData.setMsg("删除成功");
        } else {
            responseData.setCode(0);
            responseData.setMsg("删除失败");
        }
        return responseData;
    }

    /**
     * 修改资源
     * @param question
     * @param request
     * @return
     */
    @RequestMapping("/OaNew/questionupdate.do")
    public ResponseData updeteResource(Question question, HttpServletRequest request) {

        User user = (User) request.getSession().getAttribute("user");
        String no = user.getNo();
        question.setuNo(no);
        boolean result = resourceService.updateResource(question);
        ResponseData responseData = new ResponseData();



        if (result) {
            responseData.setCode(1);
            responseData.setMsg("修改成功");
        } else {
            responseData.setCode(0);
            responseData.setMsg("修改失败");
        }
        return responseData;
    }


}
