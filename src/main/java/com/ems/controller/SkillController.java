package com.ems.controller;

import com.ems.entity.ResponseData;
import com.ems.entity.Skill;
import com.ems.entity.User;
import com.ems.service.SkillService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class SkillController {
    @Autowired
    private SkillService skillService;
    @RequestMapping("/user/query.do")
    public ResponseData getUserNo(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        ResponseData responseData = new ResponseData();
        responseData.setCode(0);
        responseData.setData(user.getNo());
        return responseData;
    }
    @RequestMapping("/skill/add.do")
    public ResponseData addSkill(String content) {
        int i = skillService.addSkill(content);
        ResponseData responseData = new ResponseData();

        if (i > 0) {
            responseData.setMsg("success");
            responseData.setCode(0);
        }else {
            responseData.setMsg("fail");
            responseData.setCode(1);
        }
        return responseData;
    }
    @RequestMapping("/skill/querySkill.do")
    public ResponseData<Skill> findAllSkill(int page, int limit) {
        PageHelper.startPage(page,limit);
        List<Skill> skillList = skillService.findAllSkill();
        PageInfo<Skill> pageInfo = new PageInfo<>(skillList);
        int count = (int) pageInfo.getTotal();
        ResponseData responseData = new ResponseData();
        responseData.setCode(0);
        responseData.setMsg("success");
        responseData.setCount(count);
        responseData.setData(skillList);
        return responseData;
    }
}
